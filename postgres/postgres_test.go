package postgres

import (
	"fmt"
	"net/url"
	"os"
	"testing"
	"time"

	retry "github.com/giantswarm/retry-go"
	"github.com/ory/dockertest"
	"github.com/shopspring/decimal"
	"github.com/stretchr/testify/assert"

	"gitlab.com/eikespang/cotef3/model"
)

var postgreqURL *url.URL
var store *Store

func initializePostgres() (*dockertest.Pool, *dockertest.Resource, error) {
	database := "cote"
	user := "postgres"
	password := "password"
	port := 5432

	pool, err := dockertest.NewPool("")
	if err != nil {
		return nil, nil, err
	}
	runOpts := dockertest.RunOptions{
		Repository: "postgres",
		Tag:        "latest",
		Env: []string{
			"POSTGRES_USER=" + user,
			"POSTGRES_PASSWORD=" + password,
			"POSTGRES_DB=" + database,
		},
	}

	postgresContainer, err := pool.RunWithOptions(&runOpts)
	if err != nil {
		return pool, nil, err
	}

	host := postgresContainer.Container.NetworkSettings.IPAddress

	postgreqURL, err = url.Parse(
		fmt.Sprintf("postgres://%s:%s@%s:%d/%s?sslmode=disable", user, password, host, port, database),
	)

	return pool, postgresContainer, err
}

func setup(s *Store) error {
	_, err := s.db.Exec(queryCreateTable)
	return err
}

func teardown(s *Store) error {
	_, err := s.db.Exec(queryDropTable)
	return err
}

func TestMain(m *testing.M) {
	pool, container, err := initializePostgres()
	if err != nil {
		fmt.Println("error occurred initialising postgres: ", err)
		return
	}
	var exitCode *int
	defer func() {
		if container != nil {
			err = pool.Purge(container)
			if err != nil {
				fmt.Println("error occurred purging postgres: ", err)
			}
		}
		os.Exit(*exitCode)
	}()

	store, err = New(postgreqURL)
	if err != nil {
		fmt.Println("error creating storage: ", err)
		*exitCode = 1
		return
	}
	err = retry.Do(
		func() error { return store.Ping() },
		retry.MaxTries(20),
		retry.Sleep(3*time.Second),
		retry.Timeout(time.Minute),
	)
	if err != nil {
		fmt.Println("couldn't establish connection to postgres: ", err)
		*exitCode = 1
		return
	}

	fmt.Println("Created storage and start tests")
	ex := m.Run()
	exitCode = &ex
}

func TestAddGetDeletePayment(t *testing.T) {
	err := setup(store)
	assert.Nil(t, err)
	defer func() {
		err := teardown(store)
		assert.Nil(t, err)
	}()

	in := model.Payment{ID: "4", Amount: decimal.NewFromFloat(2)}

	_, err = store.GetPayment(in.ID)
	assert.Error(t, err, model.ErrPaymentNotFound)

	err = store.AddPayment(in)
	assert.Nil(t, err)

	out, err := store.GetPayment(in.ID)
	assert.Nil(t, err)
	assert.Equal(t, out, in)

	err = store.AddPayment(in)
	assert.Error(t, err, model.ErrPaymentAlreadyStored)

	err = store.DeletePayment(in.ID)
	assert.Nil(t, err)

	err = store.DeletePayment(in.ID)
	assert.Error(t, err, model.ErrPaymentNotFound)
}

func TestUpdatePayment(t *testing.T) {
	err := setup(store)
	assert.Nil(t, err)
	defer func() {
		err := teardown(store)
		assert.Nil(t, err)
	}()

	in := model.Payment{ID: "4", Amount: decimal.NewFromFloat(2)}

	err = store.UpdatePayment(in)
	assert.Error(t, err, model.ErrPaymentNotFound)

	err = store.AddPayment(in)
	assert.Nil(t, err)

	in.Amount = decimal.NewFromFloat(2.2)
	err = store.UpdatePayment(in)
	assert.Nil(t, err)

	out, err := store.GetPayment(in.ID)
	assert.Nil(t, err)
	assert.Equal(t, out, in)
}

func TestGetPayments(t *testing.T) {
	err := setup(store)
	assert.Nil(t, err)
	defer func() {
		err := teardown(store)
		assert.Nil(t, err)
	}()

	first := model.Payment{ID: "3", Amount: decimal.NewFromFloat(1.2)}
	second := model.Payment{ID: "4", Amount: decimal.NewFromFloat(2.3)}

	payments, err := store.GetPayments()
	assert.Nil(t, err)
	assert.Len(t, payments, 0)

	err = store.AddPayment(first)
	assert.Nil(t, err)

	payments, err = store.GetPayments()
	assert.Nil(t, err)
	assert.Len(t, payments, 1)

	err = store.AddPayment(second)
	assert.Nil(t, err)

	payments, err = store.GetPayments()
	assert.Nil(t, err)
	assert.Len(t, payments, 2)
	assert.Subset(t, payments, []model.Payment{first, second})
}
