CREATE TYPE currency AS ENUM ('GBP');
CREATE TYPE payment_scheme AS ENUM ('FPS');

CREATE TABLE payments {
    id TEXT PRIMARY KEY,
    amount NUMERIC(20, 2) NOT NULL,
    currency currency NOT NULL,
    beneficiary_id text NOT NULL, 
    end_to_end_reference text,
    payment_id text NOT NULL,
    payment_purpose text,
    payment_scheme payment_scheme NOT NULL,
    payment_type payment_type NOT NULL,
    processing_date Date
};
