package postgres

import (
	"database/sql"
	"net/url"

	_ "github.com/lib/pq" // import postgres driver
	"github.com/shopspring/decimal"

	"gitlab.com/eikespang/cotef3/model"
)

// Store has a conneciton pool to postgres and
// is the access layer for state changes.
type Store struct {
	db *sql.DB
}

// New create a new store.
func New(url *url.URL) (*Store, error) {
	db, err := sql.Open("postgres", url.String())
	if err != nil {
		return nil, err
	}

	return &Store{
		db: db,
	}, nil
}

// Close the underlaying database.
func (s *Store) Close() error {
	return s.db.Close()
}

// Ping pings the database.
func (s *Store) Ping() error {
	return s.db.Ping()
}

// GetPayment gets the payment with id 'id' from the store
// or returns NotFound.
func (s *Store) GetPayment(id string) (model.Payment, error) {
	row := s.db.QueryRow(
		`SELECT amount FROM payments WHERE id = $1`,
		id,
	)
	var amount decimal.Decimal
	err := row.Scan(&amount)
	if err != nil {
		if err == sql.ErrNoRows {
			return model.Payment{}, model.ErrPaymentNotFound
		}
		return model.Payment{}, err
	}
	return model.Payment{
		ID:     id,
		Amount: amount,
	}, nil
}

// GetPayments returns all payments in the store.
func (s *Store) GetPayments() ([]model.Payment, error) {
	rows, err := s.db.Query(queryGetPayments)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}
		return nil, err
	}
	defer rows.Close()

	var payments []model.Payment
	for rows.Next() {
		var id string
		var amount decimal.Decimal
		err := rows.Scan(&id, &amount)
		if err != nil {
			return nil, err
		}
		payments = append(payments, model.Payment{
			ID:     id,
			Amount: amount,
		})
	}

	return payments, nil
}

// AddPayment adds a payment if doesn't exists.
// Returns AlreadyStored otherwise.
func (s *Store) AddPayment(p model.Payment) error {
	result, err := s.db.Exec(
		queryAddPayment,
		p.ID,
		p.Amount,
	)
	if err != nil {
		return err
	}

	rowsAffected, err := result.RowsAffected()
	if err != nil {
		return err
	}
	if rowsAffected == 0 {
		return model.ErrPaymentAlreadyStored
	}
	return nil
}

// UpdatePayment updates a payment from the store.
// Will return NotFound when the payment doesn't
// exist.
func (s *Store) UpdatePayment(p model.Payment) error {
	result, err := s.db.Exec(
		queryUpdatePayment,
		p.ID,
		p.Amount,
	)
	if err != nil {
		return err
	}

	rowsAffected, err := result.RowsAffected()
	if err != nil {
		return err
	}
	if rowsAffected == 0 {
		return model.ErrPaymentNotFound
	}
	return nil
}

// DeletePayment delets payment with id 'id' from the
// store or returns NotFound.
func (s *Store) DeletePayment(id string) error {
	result, err := s.db.Exec(queryDeletePayment, id)
	if err != nil {
		return err
	}
	rowsAffected, err := result.RowsAffected()
	if err != nil {
		return err
	}
	if rowsAffected == 0 {
		return model.ErrPaymentNotFound
	}
	return nil
}
