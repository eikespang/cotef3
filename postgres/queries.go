package postgres

const queryCreateTable = `
CREATE TABLE payments (
  id TEXT PRIMARY KEY,
  amount NUMERIC(20, 2) NOT NULL
)`

const queryDropTable = `
DROP TABLE IF EXISTS payments
`

const queryUpdatePayment = `
UPDATE payments
SET amount = $2
WHERE id = $1
`

const queryAddPayment = `
INSERT INTO payments (id, amount)
SELECT $1, $2
WHERE NOT EXISTS (SELECT id FROM payments WHERE id = $1)
`

const queryDeletePayment = `
DELETE FROM payments WHERE id = $1
`

const queryGetPayments = `
SELECT id, amount FROM payments
`
