package model

import "errors"

var (
	// ErrPaymentNotFound should be returned when a payment
	// doesn't exist.
	ErrPaymentNotFound = errors.New("payment not found")
	// ErrPaymentAlreadyStored is returned on create requests
	// when the payment already exists.
	ErrPaymentAlreadyStored = errors.New("payment with that id already stored")
)
