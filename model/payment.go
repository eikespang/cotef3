package model

import (
	"github.com/shopspring/decimal"
)

// Payment model.
type Payment struct {
	ID     string          `json:"payment_id"`
	Amount decimal.Decimal `json:"amount"`
}
