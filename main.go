package main

import (
	"context"
	"log"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"time"

	"github.com/gorilla/mux"
	"github.com/kelseyhightower/envconfig"
	"github.com/sirupsen/logrus"
	"gitlab.com/eikespang/cotef3/app"
	"gitlab.com/eikespang/cotef3/postgres"
)

// Options to configure the service.
type Options struct {
	Debug       bool     `envconfig:"DEBUG"`
	PostgresURL *url.URL `envconfig:"POSTGRES_URL"`
}

func main() {
	var options Options
	err := envconfig.Process("", &options)
	if err != nil {
		log.Fatal(err.Error())
	}

	logLevel := logrus.WarnLevel
	if options.Debug {
		logLevel = logrus.DebugLevel
	}
	// Setup logger for usage in the command line
	// for production this should be configureable
	// and default to json/ stdout.
	logger := &logrus.Logger{
		Out: os.Stdout,
		Formatter: &logrus.TextFormatter{
			ForceColors:     true,
			FullTimestamp:   true,
			TimestampFormat: time.StampMilli,
		},
		Level: logLevel,
	}
	store, err := postgres.New(options.PostgresURL)
	if err != nil {
		logger.WithError(err).Fatal("creating postgres store.")
	}

	router := mux.NewRouter()
	server := &app.Server{
		Store:  store,
		Router: router,
		Logger: logger,
		HTTPServer: &http.Server{
			Addr:         "0.0.0.0:8080",
			WriteTimeout: time.Second * 15,
			ReadTimeout:  time.Second * 15,
			IdleTimeout:  time.Second * 60,
			Handler:      router,
		},
	}

	// React on signals to stop the server.
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)

	go func() {
		<-c
		logger.Info("received signal to gracefully stop the server")
		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()
		err := server.Shutdown(ctx)
		if err != nil {
			logger.WithError(err).Warn("error occured in server.Shutdown")
		}
		logger.Info("shutting down")
	}()

	if err := server.Run(); err != nil {
		log.Println(err)
	}

}
