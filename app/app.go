package app

import (
	"context"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"

	"gitlab.com/eikespang/cotef3/model"
)

// PaymentStore defines an interface. Any implemetation of that
// can be used as part of the server.
type PaymentStore interface {
	GetPayment(id string) (model.Payment, error)
	GetPayments() ([]model.Payment, error)
	AddPayment(model.Payment) error
	UpdatePayment(model.Payment) error
	DeletePayment(id string) error
	Ping() error
}

// Server collects all shared dependencies.
type Server struct {
	Logger     *logrus.Logger
	HTTPServer *http.Server
	Router     *mux.Router
	Store      PaymentStore
}

// Run starts the server.
func (s *Server) Run() error {
	s.routes()
	return s.HTTPServer.ListenAndServe()
}

// Shutdown stops the server. Timesout when the context
// times out.
func (s *Server) Shutdown(ctx context.Context) error {
	return s.HTTPServer.Shutdown(ctx)
}
