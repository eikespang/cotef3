package app

import (
	"net/http"
	"time"

	"github.com/sirupsen/logrus"
)

func (s *Server) routes() {
	s.Router.HandleFunc("/payments", s.logMiddleware(s.handleGetPayments())).Methods("GET")
	s.Router.HandleFunc("/payments/{payment_id}", s.logMiddleware(s.handleGetPayment())).Methods("GET")
	s.Router.HandleFunc("/payments", s.logMiddleware(s.handleCreatePayment())).Methods("POST")
	s.Router.HandleFunc("/payments", s.logMiddleware(s.handleUpdatePayment())).Methods("PUT")
	s.Router.HandleFunc("/payments/{payment_id}", s.logMiddleware(s.handleDeletePayment())).Methods("DELETE")

	// healthchecks
	s.Router.HandleFunc("/live", s.handleLive()).Methods("GET")
	s.Router.HandleFunc("/ready", s.handleReady()).Methods("GET")
}

func (s *Server) logMiddleware(h http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()
		h(w, r)
		s.Logger.WithFields(
			logrus.Fields{
				"route": r.RequestURI,
				"took":  time.Since(start),
			},
		).Debug("Request handled")
	}
}
