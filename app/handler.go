package app

import (
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"gitlab.com/eikespang/cotef3/model"
)

func (s *Server) handleGetPayments() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		payments, err := s.Store.GetPayments()
		if err != nil {
			s.Logger.WithError(err).WithFields(
				logrus.Fields{
					"route": r.RequestURI,
				}).Error("internal error")
			http.Error(w, "internal server error", http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		_ = json.NewEncoder(w).Encode(payments)
	}
}

func (s *Server) handleGetPayment() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		id := vars["payment_id"]

		payment, err := s.Store.GetPayment(id)
		if err != nil {
			if err == model.ErrPaymentNotFound {
				http.Error(w, err.Error(), http.StatusNotFound)
				return
			}
			s.Logger.WithError(err).WithFields(
				logrus.Fields{
					"route":      r.RequestURI,
					"paymend_id": id,
				}).Error("internal error")
			http.Error(w, "internal server error", http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		_ = json.NewEncoder(w).Encode(payment)
	}
}

func (s *Server) handleCreatePayment() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		decoder := json.NewDecoder(r.Body)
		defer r.Body.Close()

		var p model.Payment
		err := decoder.Decode(&p)
		if err != nil {
			http.Error(w, "error decoding request", http.StatusBadRequest)
			return
		}

		err = s.Store.AddPayment(p)
		if err != nil {
			if err == model.ErrPaymentAlreadyStored {
				http.Error(w, err.Error(), http.StatusConflict)
				return
			}
			s.Logger.WithError(err).WithField("route", r.RequestURI).Error("internal error")
			http.Error(w, "internal server error", http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusCreated)
	}
}

func (s *Server) handleUpdatePayment() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		decoder := json.NewDecoder(r.Body)
		defer r.Body.Close()

		var p model.Payment
		err := decoder.Decode(&p)
		if err != nil {
			http.Error(w, "error decoding request", http.StatusBadRequest)
			return
		}

		err = s.Store.UpdatePayment(p)
		if err != nil {
			if err == model.ErrPaymentNotFound {
				http.Error(w, err.Error(), http.StatusNotFound)
				return
			}
			s.Logger.WithError(err).WithField("route", r.RequestURI).Error("internal error")
			http.Error(w, "internal server error", http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
	}
}

func (s *Server) handleDeletePayment() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		id := vars["payment_id"]

		err := s.Store.DeletePayment(id)
		if err != nil {
			if err == model.ErrPaymentNotFound {
				http.Error(w, err.Error(), http.StatusNotFound)
				return
			}
			s.Logger.WithError(err).WithFields(
				logrus.Fields{
					"route":      r.RequestURI,
					"paymend_id": id,
				}).Error("internal error")
			http.Error(w, "internal server error", http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
	}
}

func (s *Server) handleLive() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
	}
}

func (s *Server) handleReady() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		err := s.Store.Ping()
		if err != nil {
			s.Logger.WithError(err).WithFields(
				logrus.Fields{
					"route": r.RequestURI,
				}).Error("internal error")
			http.Error(w, "internal server error", http.StatusInternalServerError)
			return
		}
	}
}
