package app

import (
	"errors"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/gorilla/mux"
	"github.com/shopspring/decimal"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/eikespang/cotef3/model"
)

type MyMockedStore struct {
	mock.Mock
}

func (m *MyMockedStore) GetPayment(id string) (model.Payment, error) {
	args := m.Called(id)
	payment, ok := args.Get(0).(model.Payment)
	if !ok {
		panic("set first argument to model.Payment in GetPayment mock")
	}
	return payment, args.Error(1)
}

func (m *MyMockedStore) GetPayments() ([]model.Payment, error) {
	args := m.Called()

	firstArg := args.Get(0)
	payments, ok := firstArg.([]model.Payment)
	if firstArg != nil && !ok {
		panic("set first argument to []model.Payment or nil in GetPayments mock")
	}
	return payments, args.Error(1)
}

func (m *MyMockedStore) AddPayment(p model.Payment) error {
	args := m.Called(p)
	return args.Error(0)
}

func (m *MyMockedStore) UpdatePayment(p model.Payment) error {
	args := m.Called(p)
	return args.Error(0)
}

func (m *MyMockedStore) DeletePayment(id string) error {
	args := m.Called(id)
	return args.Error(0)
}

func (m *MyMockedStore) Ping() error {
	args := m.Called()
	return args.Error(0)
}

func invokeServerWith(req *http.Request, testStore *MyMockedStore) *httptest.ResponseRecorder {
	logger := logrus.New()
	logger.Out = ioutil.Discard
	srv := &Server{
		Store:  testStore,
		Router: mux.NewRouter(),
		Logger: logger,
	}
	srv.routes()
	w := httptest.NewRecorder()
	srv.Router.ServeHTTP(w, req)
	return w
}

func TestGetPaymentSuccess(t *testing.T) {
	testStore := new(MyMockedStore)
	testStore.On("GetPayment", "1").Return(model.Payment{ID: "1", Amount: decimal.NewFromFloat(2)}, nil)

	req, err := http.NewRequest("GET", "/payments/1", nil)
	assert.Nil(t, err)

	w := invokeServerWith(req, testStore)
	assert.Equal(t, http.StatusOK, w.Code)

	want := `{"payment_id":"1","amount":"2"}
`
	assert.Equal(t, want, w.Body.String())
	testStore.AssertExpectations(t)
}

func TestGetPaymentNotFound(t *testing.T) {
	testStore := new(MyMockedStore)
	testStore.On("GetPayment", "1").Return(model.Payment{}, model.ErrPaymentNotFound)

	req, err := http.NewRequest("GET", "/payments/1", nil)
	assert.Nil(t, err)

	w := invokeServerWith(req, testStore)
	assert.Equal(t, http.StatusNotFound, w.Code)

	testStore.AssertExpectations(t)
}

func TestGetPaymentInternal(t *testing.T) {
	testStore := new(MyMockedStore)
	testStore.On("GetPayment", "1").Return(model.Payment{}, errors.New("database is not available"))

	req, err := http.NewRequest("GET", "/payments/1", nil)
	assert.Nil(t, err)

	w := invokeServerWith(req, testStore)
	assert.Equal(t, http.StatusInternalServerError, w.Code)

	testStore.AssertExpectations(t)
}

func TestGetPaymentsSuccess(t *testing.T) {
	testStore := new(MyMockedStore)
	testStore.On("GetPayments").Return(
		[]model.Payment{
			{
				ID:     "1",
				Amount: decimal.NewFromFloat(2),
			},
			{
				ID:     "2",
				Amount: decimal.NewFromFloat(3),
			},
		},
		nil,
	)

	req, err := http.NewRequest("GET", "/payments", nil)
	assert.Nil(t, err)

	w := invokeServerWith(req, testStore)
	assert.Equal(t, http.StatusOK, w.Code)

	want := `[{"payment_id":"1","amount":"2"},{"payment_id":"2","amount":"3"}]
`
	assert.Equal(t, want, w.Body.String())
	testStore.AssertExpectations(t)
}

func TestGetPaymentsInternal(t *testing.T) {
	testStore := new(MyMockedStore)
	testStore.On("GetPayments").Return(nil, errors.New("database is not available"))

	req, err := http.NewRequest("GET", "/payments", nil)
	assert.Nil(t, err)

	w := invokeServerWith(req, testStore)
	assert.Equal(t, http.StatusInternalServerError, w.Code)
	testStore.AssertExpectations(t)
}

func TestPostPaymentsSuccess(t *testing.T) {
	testStore := new(MyMockedStore)
	testStore.On("AddPayment", model.Payment{ID: "1", Amount: decimal.NewFromFloat(2)}).Return(nil)
	req, err := http.NewRequest("POST", "/payments", strings.NewReader(`{"payment_id":"1","amount":2}`))
	assert.Nil(t, err)

	w := invokeServerWith(req, testStore)
	assert.Equal(t, http.StatusCreated, w.Code)
	testStore.AssertExpectations(t)
}

func TestPostPaymentsInvalidJson(t *testing.T) {
	testStore := new(MyMockedStore)
	req, err := http.NewRequest("POST", "/payments", strings.NewReader(`{"payment_id":"1","amount":2`))
	assert.Nil(t, err)
	w := invokeServerWith(req, testStore)
	assert.Equal(t, http.StatusBadRequest, w.Code)
	testStore.AssertExpectations(t)
}

func TestPostPaymentsConflict(t *testing.T) {
	testStore := new(MyMockedStore)
	testStore.On("AddPayment", model.Payment{ID: "1", Amount: decimal.NewFromFloat(2)}).Return(model.ErrPaymentAlreadyStored)
	req, err := http.NewRequest("POST", "/payments", strings.NewReader(`{"payment_id":"1","amount":2}`))
	assert.Nil(t, err)

	w := invokeServerWith(req, testStore)
	assert.Equal(t, http.StatusConflict, w.Code)
	testStore.AssertExpectations(t)
}

func TestPostPaymentsInternal(t *testing.T) {
	testStore := new(MyMockedStore)
	testStore.On("AddPayment", model.Payment{ID: "1", Amount: decimal.NewFromFloat(2)}).Return(errors.New("database is not available"))
	req, err := http.NewRequest("POST", "/payments", strings.NewReader(`{"payment_id":"1","amount":2}`))
	assert.Nil(t, err)

	w := invokeServerWith(req, testStore)
	assert.Equal(t, http.StatusInternalServerError, w.Code)
	testStore.AssertExpectations(t)
}

func TestPutPaymentsSuccess(t *testing.T) {
	testStore := new(MyMockedStore)
	testStore.On("UpdatePayment", model.Payment{ID: "1", Amount: decimal.NewFromFloat(3)}).Return(nil)
	req, err := http.NewRequest("PUT", "/payments", strings.NewReader(`{"payment_id":"1","amount":3}`))
	assert.Nil(t, err)

	w := invokeServerWith(req, testStore)
	assert.Equal(t, http.StatusOK, w.Code)
	testStore.AssertExpectations(t)
}

func TestPutPaymentsInvalidJson(t *testing.T) {
	testStore := new(MyMockedStore)
	req, err := http.NewRequest("PUT", "/payments", strings.NewReader(`{"payment_id":"1","amount":3`))
	assert.Nil(t, err)
	w := invokeServerWith(req, testStore)
	assert.Equal(t, http.StatusBadRequest, w.Code)
	testStore.AssertExpectations(t)
}

func TestPutPaymentsNotFound(t *testing.T) {
	testStore := new(MyMockedStore)
	testStore.On("UpdatePayment", model.Payment{ID: "1", Amount: decimal.NewFromFloat(3)}).Return(model.ErrPaymentNotFound)
	req, err := http.NewRequest("PUT", "/payments", strings.NewReader(`{"payment_id":"1","amount":3}`))
	assert.Nil(t, err)

	w := invokeServerWith(req, testStore)
	assert.Equal(t, http.StatusNotFound, w.Code)
	testStore.AssertExpectations(t)
}

func TestPutPaymentsInternal(t *testing.T) {
	testStore := new(MyMockedStore)
	testStore.On("UpdatePayment", model.Payment{ID: "1", Amount: decimal.NewFromFloat(3)}).Return(errors.New("database is not available"))
	req, err := http.NewRequest("PUT", "/payments", strings.NewReader(`{"payment_id":"1","amount":3}`))
	assert.Nil(t, err)

	w := invokeServerWith(req, testStore)
	assert.Equal(t, http.StatusInternalServerError, w.Code)
	testStore.AssertExpectations(t)
}

func TestDeletePaymentsSuccess(t *testing.T) {
	testStore := new(MyMockedStore)
	testStore.On("DeletePayment", "1").Return(nil)
	req, err := http.NewRequest("DELETE", "/payments/1", nil)
	assert.Nil(t, err)

	w := invokeServerWith(req, testStore)
	assert.Equal(t, http.StatusOK, w.Code)
	testStore.AssertExpectations(t)
}

func TestDeletePaymentsNotFound(t *testing.T) {
	testStore := new(MyMockedStore)
	testStore.On("DeletePayment", "1").Return(model.ErrPaymentNotFound)
	req, err := http.NewRequest("DELETE", "/payments/1", nil)
	assert.Nil(t, err)

	w := invokeServerWith(req, testStore)
	assert.Equal(t, http.StatusNotFound, w.Code)
	testStore.AssertExpectations(t)
}

func TestDeletePaymentsInternal(t *testing.T) {
	testStore := new(MyMockedStore)
	testStore.On("DeletePayment", "1").Return(errors.New("database is not available"))
	req, err := http.NewRequest("DELETE", "/payments/1", nil)
	assert.Nil(t, err)

	w := invokeServerWith(req, testStore)
	assert.Equal(t, http.StatusInternalServerError, w.Code)
	testStore.AssertExpectations(t)
}

func TestLive(t *testing.T) {
	testStore := new(MyMockedStore)
	req, err := http.NewRequest("GET", "/live", nil)
	assert.Nil(t, err)

	w := invokeServerWith(req, testStore)
	assert.Equal(t, http.StatusOK, w.Code)
	testStore.AssertExpectations(t)
}

func TestReady(t *testing.T) {
	testStore := new(MyMockedStore)
	testStore.On("Ping").Return(nil)

	req, err := http.NewRequest("GET", "/ready", nil)
	assert.Nil(t, err)

	w := invokeServerWith(req, testStore)
	assert.Equal(t, http.StatusOK, w.Code)
	testStore.AssertExpectations(t)
}

func TestReadyInternal(t *testing.T) {
	testStore := new(MyMockedStore)
	testStore.On("Ping").Return(errors.New("database is not available"))

	req, err := http.NewRequest("GET", "/ready", nil)
	assert.Nil(t, err)

	w := invokeServerWith(req, testStore)
	assert.Equal(t, http.StatusInternalServerError, w.Code)
	testStore.AssertExpectations(t)
}
