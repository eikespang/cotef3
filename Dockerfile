FROM golang:1.12.0-alpine AS builder

RUN apk update && apk add --no-cache git
WORKDIR /go/src/gitlab.com/eikespang/cotef3
COPY . .

RUN go get -d -v
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags="-w -s" -o cotef3

FROM scratch 

COPY --from=builder /go/src/gitlab.com/eikespang/cotef3/cotef3 /cotef3

ENTRYPOINT ["/cotef3"]
